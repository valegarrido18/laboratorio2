#include <iostream>
#include <stdlib.h>

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
	private:
		int dimension = 4;
		int *pila = NULL;
		int tope = 0;
		bool BAND=true;
		
	public: 
		/* Constructor*/
		Pila ();
		void Pila_vacia();
		void pila_llena();
		void Push();
		void Pop();
		void ver_datos();
};
#endif
